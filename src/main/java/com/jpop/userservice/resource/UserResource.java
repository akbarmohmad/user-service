package com.jpop.userservice.resource;


import com.jpop.userservice.dto.UserDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/users")
@Api(tags = {"Users: UserResource"})
public interface UserResource {

    @ApiOperation(value = "View list of available users", response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved users"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @GetMapping
    ResponseEntity<List<UserDto>> getUsers();

    @ApiOperation(value = "get a user by userId", response = UserDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved user"),
            @ApiResponse(code = 404, message = "User not found")
    })
    @GetMapping("/{user_id}")
    ResponseEntity<UserDto> getUser(@PathVariable(value = "user_id") Long userId);

    @ApiOperation(value = "create a user", response = UserDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully created a user"),
            @ApiResponse(code = 404, message = "Resource not found")
    })
    @PostMapping
    ResponseEntity<UserDto> addUser( @Valid @RequestBody UserDto UserDto);

    @ApiOperation(value = "update user", response = UserDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully updated user"),
            @ApiResponse(code = 404, message = "User not found")
    })
    @PutMapping("/{user_id}")
    ResponseEntity<UserDto> updateUser(@PathVariable(value = "user_id") Long userId, @Valid @RequestBody UserDto userDetails);

    @ApiOperation(value = "delete user by userId")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully deleted user"),
            @ApiResponse(code = 404, message = "User not found")
    })
    @DeleteMapping("/{user_id}")
    ResponseEntity deleteUser(@PathVariable(value = "user_id") Long userId);
}
