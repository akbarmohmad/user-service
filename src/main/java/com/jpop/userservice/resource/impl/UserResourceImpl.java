package com.jpop.userservice.resource.impl;

import com.jpop.userservice.dto.UserDto;
import com.jpop.userservice.exception.UserNotFoundException;
import com.jpop.userservice.model.User;
import com.jpop.userservice.repository.UserRepository;
import com.jpop.userservice.resource.UserResource;
import org.modelmapper.ModelMapper;
import org.springframework.http.ResponseEntity;

import javax.inject.Inject;
import javax.inject.Named;
import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;
@Named
public class UserResourceImpl implements UserResource {

    private static final String NO_USER_FOUND_FOR_ID = "No User Found for id- ";

    @Inject
    private UserRepository userRepository;

    @Inject
    private ModelMapper modelMapper;

    @Override
    public ResponseEntity<List<UserDto>> getUsers() {
        List<User> users = userRepository.findAll();
        List<UserDto> userDtos = users.stream().map(this::convertToDto).collect(Collectors.toList());
        return ResponseEntity.ok(userDtos);
    }

    @Override
    public ResponseEntity<UserDto> getUser(Long userId) {
        return userRepository.findById(userId).map(user -> ResponseEntity.ok(convertToDto(user)))
                .orElseThrow(() -> new UserNotFoundException(NO_USER_FOUND_FOR_ID + userId));
    }

    @Override
    public ResponseEntity<UserDto> addUser(@Valid UserDto userDto) {
        User newUser = userRepository.save(modelMapper.map(userDto, User.class));
        return ResponseEntity.ok(convertToDto(newUser));
    }

    @Override
    public ResponseEntity<UserDto> updateUser(Long userId, @Valid UserDto userDetails) {
        User user = userRepository.findById(userId).orElseThrow(() -> new UserNotFoundException(NO_USER_FOUND_FOR_ID + userId));
        user.setFirstName(userDetails.getFirstName());
        user.setLastName(userDetails.getLastName());
        user.setContact(userDetails.getContact());
        userRepository.save(user);
        return ResponseEntity.ok(convertToDto(user));
    }

    private UserDto convertToDto(User user) {
        return modelMapper.map(user, UserDto.class);
    }

    @Override
    public ResponseEntity deleteUser(Long userId) {
        return userRepository.findById(userId).map(user -> {
            userRepository.delete(user);
            return ResponseEntity.ok().build();
        }).orElseThrow(() -> new UserNotFoundException(NO_USER_FOUND_FOR_ID + userId));
    }
}
