CREATE TABLE USER (
  user_id BIGINT(30) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  user_name varchar(50) NOT NULL,
  first_name varchar(50) NOT NULL,
  middle_name varchar(50) DEFAULT NULL,
  last_name varchar(50) NOT NULL,
  email varchar(50) NOT NULL,
  contact varchar(20) NOT NULL,
  created_date TIMESTAMP NOT NULL,
  updated_date TIMESTAMP NOT NULL,
  UNIQUE KEY UK_username (user_name)
) DEFAULT CHARSET=utf8;